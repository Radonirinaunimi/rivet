# BEGIN PLOT /CMS_2011_I902309/d.*
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
LegendYPos=0.9
# END PLOT
# BEGIN PLOT /CMS_2011_I902309/d01-x01-y01
Title=Inclusive jets, $0.0 < |y| < 0.5$
# END PLOT
# BEGIN PLOT /CMS_2011_I902309/d02-x01-y01
Title=Inclusive jets, $0.5 < |y| < 1.0$
# END PLOT
# BEGIN PLOT /CMS_2011_I902309/d03-x01-y01
Title=Inclusive jets, $1.0 < |y| < 1.5$
# END PLOT
# BEGIN PLOT /CMS_2011_I902309/d04-x01-y01
Title=Inclusive jets, $1.5 < |y| < 2.0$
# END PLOT
# BEGIN PLOT /CMS_2011_I902309/d05-x01-y01
Title=Inclusive jets, $2.0 < |y| < 2.5$
# END PLOT
# BEGIN PLOT /CMS_2011_I902309/d06-x01-y01
Title=Inclusive jets, $2.5 < |y| < 3.0$
# END PLOT

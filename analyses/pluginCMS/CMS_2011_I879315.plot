# BEGIN PLOT /CMS_2011_I879315/d
XLabel=n
YLabel=$P_{n}$
LegendXPos=0.7
FullRange=1
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d2[3-5]
XLabel=n
YLabel=$\langle p_\perp \rangle$ [GeV]
LegendXPos=0.5
LogY=0
# END PLOT


# BEGIN PLOT /CMS_2011_I879315/d02-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 0.5$, $\sqrt{s} = 0.9\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d03-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 1.0$, $\sqrt{s} = 0.9\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d04-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 1.5$, $\sqrt{s} = 0.9\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d05-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 2.0$, $\sqrt{s} = 0.9\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d06-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 2.4$, $\sqrt{s} = 0.9\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d07-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 0.5$, $\sqrt{s} = 2.36\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d08-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 1.0$, $\sqrt{s} = 2.36\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d09-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 1.5$, $\sqrt{s} = 2.36\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d10-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 2.0$, $\sqrt{s} = 2.36\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d11-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 2.4$, $\sqrt{s} = 2.36\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d12-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 0.5$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d13-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 1.0$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d14-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 1.5$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d15-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 2.0$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d16-x01-y01
Title=Charged hadron multiplicity, $|\eta| < 2.4$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d20-x01-y01
Title=Charged hadron multiplicity, $p_\perp > 500\,\mathrm{GeV}$, $|\eta| < 2.4$, $\sqrt{s} = 0.9\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d21-x01-y01
Title=Charged hadron multiplicity, $p_\perp > 500\,\mathrm{GeV}$, $|\eta| < 2.4$, $\sqrt{s} = 2.36\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d22-x01-y01
Title=Charged hadron multiplicity, $p_\perp > 500\,\mathrm{GeV}$, $|\eta| < 2.4$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d23-x01-y01
Title=Mean $p_\perp$ vs charged hadron multiplicity, $|\eta| < 2.4$, $\sqrt{s} = 0.9\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d24-x01-y01
Title=Mean $p_\perp$ vs charged hadron multiplicity, $|\eta| < 2.4$, $\sqrt{s} = 2.36\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2011_I879315/d25-x01-y01
Title=Mean $p_\perp$ vs charged hadron multiplicity, $|\eta| < 2.4$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT


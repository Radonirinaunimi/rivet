BEGIN PLOT /BESIII_2022_I2102455
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d01-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D^0\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d01-x01-y02
Title=$\pi^-\pi^0$ mass distribution in $D^0\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d01-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^0\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT

BEGIN PLOT /BESIII_2022_I2102455/d02-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D^0\to \pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d02-x01-y02
Title=$\pi^-\pi^0$ mass distribution in $D^0\to \pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d02-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^0\to \pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d02-x01-y04
Title=$\pi^0\pi^0$ mass distribution in $D^0\to \pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d02-x01-y05
Title=$\pi^+\pi^0\pi^0$ mass distribution in $D^0\to \pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d02-x01-y06
Title=$\pi^-\pi^0\pi^0$ mass distribution in $D^0\to \pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^-\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d02-x01-y07
Title=$\pi^+\pi^-\pi^0$ mass distribution in $D^0\to \pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT

BEGIN PLOT /BESIII_2022_I2102455/d03-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d03-x01-y02
Title=$\pi^-\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d03-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d03-x01-y04
Title=$\pi^+\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d03-x01-y05
Title=$\pi^+\pi^-\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d03-x01-y06
Title=$\pi^+\pi^-\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d03-x01-y07
Title=$\pi^+\pi^+\pi^-\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d03-x01-y08
Title=$\pi^+\pi^-\pi^-\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d03-x01-y09
Title=$\pi^+\pi^+\pi^-\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
END PLOT

BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y02
Title=$\pi^-\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y04
Title=$\pi^0\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y05
Title=$\pi^+\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y06
Title=$\pi^+\pi^-\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y07
Title=$\pi^+\pi^-\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y08
Title=$\pi^+\pi^+\pi^-\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y09
Title=$\pi^+\pi^-\pi^-\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y10
Title=$\pi^+\pi^+\pi^-\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y11
Title=$\pi^+\pi^-\pi^0\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^0\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y12
Title=$\pi^+\pi^+\pi^-\pi^0\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-\pi^0\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y13
Title=$\pi^+\pi^-\pi^-\pi^0\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^-\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^-\pi^0\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d04-x01-y14
Title=$\pi^+\pi^+\pi^-\pi^-\pi^0$ mass distribution in $D^0\to 2\pi^+2\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT

BEGIN PLOT /BESIII_2022_I2102455/d05-x01-y01
Title=$\pi^+\pi^+$ mass distribution in $D^+\to 2\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d05-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $D^+\to 2\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT

BEGIN PLOT /BESIII_2022_I2102455/d06-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D^+\to \pi^+2\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d06-x01-y02
Title=$\pi^0\pi^0$ mass distribution in $D^+\to \pi^+2\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}$]
END PLOT


BEGIN PLOT /BESIII_2022_I2102455/d07-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D^+\to 2\pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d07-x01-y02
Title=$\pi^-\pi^0$ mass distribution in $D^+\to 2\pi^+\pi^-\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d07-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^+\to 2\pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d07-x01-y04
Title=$\pi^+\pi^+\pi^-$ mass distribution in $D^+\to 2\pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d07-x01-y05
Title=$\pi^+\pi^-\pi^0$ mass distribution in $D^+\to 2\pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT

BEGIN PLOT /BESIII_2022_I2102455/d08-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $D^+\to 3\pi^+2\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d08-x01-y02
Title=$\pi^+\pi^+\pi^-$ mass distribution in $D^+\to 3\pi^+2\pi^-$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d08-x01-y03
Title=$\pi^+\pi^-\pi^-$ mass distribution in $D^+\to 3\pi^+2\pi^-$
XLabel=$m_{\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d08-x01-y04
Title=$\pi^+\pi^+\pi^+\pi^-$ mass distribution in $D^+\to 3\pi^+2\pi^-$
XLabel=$m_{\pi^+\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT

BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y02
Title=$\pi^-\pi^0$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y04
Title=$\pi^+\pi^+\pi^-$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y05
Title=$\pi^+\pi^-\pi^-$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y06
Title=$\pi^+\pi^-\pi^0$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y07
Title=$\pi^+\pi^+\pi^-\pi^0$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y08
Title=$\pi^+\pi^-\pi^-\pi^0$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y09
Title=$\pi^+\pi^+\pi^-\pi^-$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y10
Title=$\pi^+\pi^+\pi^+\pi^-\pi^-$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^+\pi^-\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2102455/d09-x01-y11
Title=$\pi^+\pi^+\pi^-\pi^-\pi^0$ mass distribution in $D^+\to 3\pi^+2\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^+\pi^-\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-\pi^-\pi^0}$ [$\text{GeV}$]
END PLOT

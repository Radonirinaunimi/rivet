BEGIN PLOT /BESIII_2022_I2611489/d01-x01-y01
Title=Positron spectrum in semileptonic $\Lambda_c^+$ decay
XLabel=$p$ [GeV]
YLabel=$1/\Gamma_{\mathrm{total}}\mathrm{d}\Gamma/\mathrm{d}p$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

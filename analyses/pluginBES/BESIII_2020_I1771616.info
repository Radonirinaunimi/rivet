Name: BESIII_2020_I1771616
Year: 2020
Summary:  Dalitz plot analysis of $\psi(2S)\to K^+K^-\eta$
Experiment: BESIII
Collider: BEPC
InspireID: 1771616
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 101 (2020) 3, 032008
RunInfo: Any process producing psi(2S) originally e+e-
Description:
  'Measurement of the mass distributions in the decay $\psi(2S)\to K^+K^-\eta$. The data were read from the plots in the paper. Also the sideband background from the plots has been subtracted. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events using model from paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2019dme
BibTeX: '@article{BESIII:2019dme,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Partial wave analysis of $\psi(3686)\to K^{+}K^{-}\eta$}",
    eprint = "1912.08566",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.101.032008",
    journal = "Phys. Rev. D",
    volume = "101",
    number = "3",
    pages = "032008",
    year = "2020"
}
'

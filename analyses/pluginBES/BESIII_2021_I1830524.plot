BEGIN PLOT /BESIII_2021_I1830524/d01-x01-y01
Title=$K^+K^-$ mass distribution in $D_s^+\to K^+K^-\pi^+$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1830524/d01-x01-y02
Title=$K^+K^-$ mass distribution in $D_s^+\to K^+K^-\pi^+$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1830524/d01-x01-y03
Title=$K^-\pi^+$ mass distribution in $D_s^+\to K^+K^-\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1830524/d01-x01-y04
Title=$K^+\pi^+$ mass distribution in $D_s^+\to K^+K^-\pi^+$
XLabel=$m^2_{K^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1830524/dalitz
Title=Dalitz plot for $D_s^+\to K^+K^-\pi^+$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+K^-}/{\rm d}m^2_{K^-\pi^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

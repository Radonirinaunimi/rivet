BEGIN PLOT /BESIII_2022_I2167804/d01-x01-y01
Title=$\Lambda\eta$ mass distribution in $\psi(2S)\to \Lambda\bar{\Lambda}\eta$
XLabel=$m_{\Lambda\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2167804/d01-x01-y02
Title=$\bar{\Lambda}\eta$ mass distribution in $\psi(2S)\to \Lambda\bar{\Lambda}\eta$
XLabel=$m_{\bar{\Lambda}\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{\Lambda}\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2167804/d01-x01-y03
Title=$\Lambda\bar{\Lambda}$ mass distribution in $\psi(2S)\to \Lambda\bar{\Lambda}\eta$
XLabel=$m_{\Lambda\bar{\Lambda}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{\Lambda\Lambda}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2167804/dalitz
Title=Dalitz plot for  $\psi(2S)\to \Lambda\bar{\Lambda}\eta$
XLabel=$m^2_{\Lambda\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{\Lambda}\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\Lambda\eta}/{\rm d}m^2_{\bar{\Lambda}\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

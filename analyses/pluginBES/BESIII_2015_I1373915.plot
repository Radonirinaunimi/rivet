BEGIN PLOT /BESIII_2015_I1373915/d01-x01-y01
Title= $\pi^0\pi^0$ mass in $J/\psi\to\gamma \pi^0\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT

Name: BESIII_2021_I1845444
Year: 2021
Summary: Mass distributions in the decay $D^+_s\to K^0_SK^-\pi^+\pi^+$
Experiment: BESIII
Collider: BEPC
InspireID: 1845444
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 103 (2021) 9, 092006
Description:
  'Measurement of the mass distributions in the decay $D^+_s\to K^0_SK^-\pi^+\pi^+$ by BES. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021dot
BibTeX: '@article{BESIII:2021dot,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Amplitude analysis and branching-fraction measurement of $D^{+}_{s}\rightarrow K^{0}_{S}K^{-}\pi^{+}\pi^{+}$}",
    eprint = "2102.03808",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.103.092006",
    journal = "Phys. Rev. D",
    volume = "103",
    number = "9",
    pages = "092006",
    year = "2021"
}
'

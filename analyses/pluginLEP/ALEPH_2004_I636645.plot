# BEGIN PLOT /ALEPH_2004_I636645/d01-x01-y01
Title=Charged multiplicity at a function of energy
XLabel=$E_\mathrm{CMS}/GeV$
YLabel=$N_\mathrm{ch}$
LegendXPos=0.20
LegendYPos=0.85
LogY=0
YMin=18
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d02-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=133$ GeV)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d03-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=161$ GeV)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d04-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=172$ GeV)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d05-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=183$ GeV)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d06-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=189$ GeV)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d07-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=196$ GeV)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d08-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=200$ GeV)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d09-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=206$ GeV)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d11-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\xi_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\xi_p$
FullRange=1
LegendYPos=0.65
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d12-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\xi_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\xi_p$
FullRange=1
LegendYPos=0.65
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d13-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\xi_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\xi_p$
FullRange=1
LegendYPos=0.65
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d14-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\xi_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\xi_p$
FullRange=1
LegendYPos=0.65
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d15-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=189$ GeV)
XLabel=$\xi_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\xi_p$
FullRange=1
LegendYPos=0.65
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d16-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=196$ GeV)
XLabel=$\xi_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\xi_p$
FullRange=1
LegendYPos=0.65
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d17-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=200$ GeV)
XLabel=$\xi_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\xi_p$
FullRange=1
LegendYPos=0.65
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d18-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=206$ GeV)
XLabel=$\xi_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\xi_p$
FullRange=1
LegendYPos=0.65
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d19-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=133$ GeV)
XLabel=$x_E$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_E}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d20-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=161$ GeV)
XLabel=$x_E$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_E}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d21-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=172$ GeV)
XLabel=$x_E$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_E}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d22-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=183$ GeV)
XLabel=$x_E$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_E}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d23-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=189$ GeV)
XLabel=$x_E$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_E}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d24-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=196$ GeV)
XLabel=$x_E$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_E}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d25-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=200$ GeV)
XLabel=$x_E$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_E}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d26-x01-y01
Title=Charged particle spectrum ($E_\mathrm{CMS}=206$ GeV)
XLabel=$x_E$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_E}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d27-x01-y01
Title=In-plane $p_\perp$ in GeV w.r.t. thrust axes ($E_\mathrm{CMS}=133$ GeV)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d28-x01-y01
Title=In-plane $p_\perp$ in GeV w.r.t. thrust axes ($E_\mathrm{CMS}=161$ GeV)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d29-x01-y01
Title=In-plane $p_\perp$ in GeV w.r.t. thrust axes ($E_\mathrm{CMS}=172$ GeV)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d30-x01-y01
Title=In-plane $p_\perp$ in GeV w.r.t. thrust axes ($E_\mathrm{CMS}=183$ GeV)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d31-x01-y01
Title=In-plane $p_\perp$ in GeV w.r.t. thrust axes ($E_\mathrm{CMS}=189$ GeV)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d32-x01-y01
Title=In-plane $p_\perp$ in GeV w.r.t. thrust axes ($E_\mathrm{CMS}=196$ GeV)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d33-x01-y01
Title=In-plane $p_\perp$ in GeV w.r.t. thrust axes ($E_\mathrm{CMS}=200$ GeV)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d34-x01-y01
Title=In-plane $p_\perp$ in GeV w.r.t. thrust axes ($E_\mathrm{CMS}=206$ GeV)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d35-x01-y01
Title=Out-of-plane $p_\perp$ in GeV w.r.t. thrust axes ($E_\mathrm{CMS}=206$ GeV)
XLabel=$p_\perp^\mathrm{out}$ / GeV
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{out}}$
FullRange=1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d36-x01-y01
Title=Rapidity w.r.t. thrust axes, $y_T$ ($E_\mathrm{CMS}=133$ GeV)
XLabel=$y_T$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_T}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d37-x01-y01
Title=Rapidity w.r.t. thrust axes, $y_T$ ($E_\mathrm{CMS}=161$ GeV)
XLabel=$y_T$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_T}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d38-x01-y01
Title=Rapidity w.r.t. thrust axes, $y_T$ ($E_\mathrm{CMS}=172$ GeV)
XLabel=$y_T$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_T}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d39-x01-y01
Title=Rapidity w.r.t. thrust axes, $y_T$ ($E_\mathrm{CMS}=183$ GeV)
XLabel=$y_T$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_T}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d40-x01-y01
Title=Rapidity w.r.t. thrust axes, $y_T$ ($E_\mathrm{CMS}=189$ GeV)
XLabel=$y_T$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_T}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d41-x01-y01
Title=Rapidity w.r.t. thrust axes, $y_T$ ($E_\mathrm{CMS}=196$ GeV)
XLabel=$y_T$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_T}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d42-x01-y01
Title=Rapidity w.r.t. thrust axes, $y_T$ ($E_\mathrm{CMS}=200$ GeV)
XLabel=$y_T$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_T}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d43-x01-y01
Title=Rapidity w.r.t. thrust axes, $y_T$ ($E_\mathrm{CMS}=206$ GeV)
XLabel=$y_T$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_T}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d44-x01-y01
Title=Rapidity w.r.t. sphericity axes, $y_S$ ($E_\mathrm{CMS}=133$ GeV)
XLabel=$y_S$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d45-x01-y01
Title=Rapidity w.r.t. sphericity axes, $y_S$ ($E_\mathrm{CMS}=161$ GeV)
XLabel=$y_S$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d46-x01-y01
Title=Rapidity w.r.t. sphericity axes, $y_S$ ($E_\mathrm{CMS}=172$ GeV)
XLabel=$y_S$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d47-x01-y01
Title=Rapidity w.r.t. sphericity axes, $y_S$ ($E_\mathrm{CMS}=183$ GeV)
XLabel=$y_S$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d48-x01-y01
Title=Rapidity w.r.t. sphericity axes, $y_S$ ($E_\mathrm{CMS}=189$ GeV)
XLabel=$y_S$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d49-x01-y01
Title=Rapidity w.r.t. sphericity axes, $y_S$ ($E_\mathrm{CMS}=196$ GeV)
XLabel=$y_S$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d50-x01-y01
Title=Rapidity w.r.t. sphericity axes, $y_S$ ($E_\mathrm{CMS}=200$ GeV)
XLabel=$y_S$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d51-x01-y01
Title=Rapidity w.r.t. sphericity axes, $y_S$ ($E_\mathrm{CMS}=206$ GeV)
XLabel=$y_S$
YLabel=$N \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
LegendXPos=0.05
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d54-x01-y01
Title=Thrust ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T$
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d55-x01-y01
Title=Thrust ($E_\mathrm{CMS}=133$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d56-x01-y01
Title=Thrust ($E_\mathrm{CMS}=161$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d57-x01-y01
Title=Thrust ($E_\mathrm{CMS}=172$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d58-x01-y01
Title=Thrust ($E_\mathrm{CMS}=183$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d59-x01-y01
Title=Thrust ($E_\mathrm{CMS}=189$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d60-x01-y01
Title=Thrust ($E_\mathrm{CMS}=200$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d61-x01-y01
Title=Thrust ($E_\mathrm{CMS}=206$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d62-x01-y01
Title=Heavy jet mass ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
LegendXPos=0.25
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d63-x01-y01
Title=Heavy jet mass ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
LegendXPos=0.65
LegendYPos=1.1
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d64-x01-y01
Title=Heavy jet mass ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d65-x01-y01
Title=Heavy jet mass ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d66-x01-y01
Title=Heavy jet mass ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d67-x01-y01
Title=Heavy jet mass ($E_\mathrm{CMS}=189$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d68-x01-y01
Title=Heavy jet mass ($E_\mathrm{CMS}=200$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d69-x01-y01
Title=Heavy jet mass ($E_\mathrm{CMS}=206$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d70-x01-y01
Title=Total jet broadening ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d71-x01-y01
Title=Total jet broadening ($E_\mathrm{CMS}=133$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d72-x01-y01
Title=Total jet broadening ($E_\mathrm{CMS}=161$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d73-x01-y01
Title=Total jet broadening ($E_\mathrm{CMS}=172$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d74-x01-y01
Title=Total jet broadening ($E_\mathrm{CMS}=183$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d75-x01-y01
Title=Total jet broadening ($E_\mathrm{CMS}=189$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d76-x01-y01
Title=Total jet broadening ($E_\mathrm{CMS}=200$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d77-x01-y01
Title=Total jet broadening ($E_\mathrm{CMS}=206$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d78-x01-y01
Title=Wide jet broadening ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d79-x01-y01
Title=Wide jet broadening ($E_\mathrm{CMS}=133$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d80-x01-y01
Title=Wide jet broadening ($E_\mathrm{CMS}=161$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d81-x01-y01
Title=Wide jet broadening ($E_\mathrm{CMS}=172$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d82-x01-y01
Title=Wide jet broadening ($E_\mathrm{CMS}=183$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d83-x01-y01
Title=Wide jet broadening ($E_\mathrm{CMS}=189$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d84-x01-y01
Title=Wide jet broadening ($E_\mathrm{CMS}=200$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d85-x01-y01
Title=Wide jet broadening ($E_\mathrm{CMS}=206$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d86-x01-y01
Title=C-Parameter ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d87-x01-y01
Title=C-Parameter ($E_\mathrm{CMS}=133$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d88-x01-y01
Title=C-Parameter ($E_\mathrm{CMS}=161$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d89-x01-y01
Title=C-Parameter ($E_\mathrm{CMS}=172$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d90-x01-y01
Title=C-Parameter ($E_\mathrm{CMS}=183$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d91-x01-y01
Title=C-Parameter ($E_\mathrm{CMS}=189$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d92-x01-y01
Title=C-Parameter ($E_\mathrm{CMS}=200$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d93-x01-y01
Title=C-Parameter ($E_\mathrm{CMS}=206$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
LegendXPos=0.15
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d94-x01-y01
Title=Thrust major ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d95-x01-y01
Title=Thrust major ($E_\mathrm{CMS}=133$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d96-x01-y01
Title=Thrust major ($E_\mathrm{CMS}=161$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d97-x01-y01
Title=Thrust major ($E_\mathrm{CMS}=172$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d98-x01-y01
Title=Thrust major ($E_\mathrm{CMS}=183$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d99-x01-y01
Title=Thrust major ($E_\mathrm{CMS}=189$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d100-x01-y01
Title=Thrust major ($E_\mathrm{CMS}=200$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d101-x01-y01
Title=Thrust major ($E_\mathrm{CMS}=206$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d102-x01-y01
Title=Thrust minor ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$\ln(T_\mathrm{minor})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(T_\mathrm{minor})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d103-x01-y01
Title=Thrust minor ($E_\mathrm{CMS}=133$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d104-x01-y01
Title=Thrust minor ($E_\mathrm{CMS}=161$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d105-x01-y01
Title=Thrust minor ($E_\mathrm{CMS}=172$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d106-x01-y01
Title=Thrust minor ($E_\mathrm{CMS}=183$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d107-x01-y01
Title=Thrust minor ($E_\mathrm{CMS}=189$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d108-x01-y01
Title=Thrust minor ($E_\mathrm{CMS}=200$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d109-x01-y01
Title=Thrust minor ($E_\mathrm{CMS}=206$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d110-x01-y01
Title=Jet mass difference ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$M_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}M_D$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d111-x01-y01
Title=Jet mass difference ($E_\mathrm{CMS}=133$ GeV)
XLabel=$M_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}M_D$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d112-x01-y01
Title=Jet mass difference ($E_\mathrm{CMS}=161$ GeV)
XLabel=$M_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}M_D$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d113-x01-y01
Title=Jet mass difference ($E_\mathrm{CMS}=172$ GeV)
XLabel=$M_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}M_D$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d114-x01-y01
Title=Jet mass difference ($E_\mathrm{CMS}=183$ GeV)
XLabel=$M_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}M_D$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d115-x01-y01
Title=Jet mass difference ($E_\mathrm{CMS}=189$ GeV)
XLabel=$M_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}M_D$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d116-x01-y01
Title=Jet mass difference ($E_\mathrm{CMS}=200$ GeV)
XLabel=$M_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}M_D$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d117-x01-y01
Title=Jet mass difference ($E_\mathrm{CMS}=206$ GeV)
XLabel=$M_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}M_D$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d118-x01-y01
Title=Aplanarity ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d119-x01-y01
Title=Aplanarity ($E_\mathrm{CMS}=133$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d120-x01-y01
Title=Aplanarity ($E_\mathrm{CMS}=161$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d121-x01-y01
Title=Aplanarity ($E_\mathrm{CMS}=172$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d122-x01-y01
Title=Aplanarity ($E_\mathrm{CMS}=183$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d123-x01-y01
Title=Aplanarity ($E_\mathrm{CMS}=189$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d124-x01-y01
Title=Aplanarity ($E_\mathrm{CMS}=200$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d125-x01-y01
Title=Aplanarity ($E_\mathrm{CMS}=206$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d126-x01-y01
Title=Planarity ($E_\mathrm{CMS}=133$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d127-x01-y01
Title=Planarity ($E_\mathrm{CMS}=161$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d128-x01-y01
Title=Planarity ($E_\mathrm{CMS}=172$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d129-x01-y01
Title=Planarity ($E_\mathrm{CMS}=183$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d130-x01-y01
Title=Planarity ($E_\mathrm{CMS}=189$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d131-x01-y01
Title=Planarity ($E_\mathrm{CMS}=200$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d132-x01-y01
Title=Planarity ($E_\mathrm{CMS}=206$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d133-x01-y01
Title=Oblateness ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d134-x01-y01
Title=Oblateness ($E_\mathrm{CMS}=133$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d135-x01-y01
Title=Oblateness ($E_\mathrm{CMS}=161$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d136-x01-y01
Title=Oblateness ($E_\mathrm{CMS}=172$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d137-x01-y01
Title=Oblateness ($E_\mathrm{CMS}=183$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d138-x01-y01
Title=Oblateness ($E_\mathrm{CMS}=189$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d139-x01-y01
Title=Oblateness ($E_\mathrm{CMS}=200$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d140-x01-y01
Title=Oblateness ($E_\mathrm{CMS}=206$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d141-x01-y01
Title=Sphericity ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d142-x01-y01
Title=Sphericity ($E_\mathrm{CMS}=133$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d143-x01-y01
Title=Sphericity ($E_\mathrm{CMS}=161$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d144-x01-y01
Title=Sphericity ($E_\mathrm{CMS}=172$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d145-x01-y01
Title=Sphericity ($E_\mathrm{CMS}=183$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d146-x01-y01
Title=Sphericity ($E_\mathrm{CMS}=189$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d147-x01-y01
Title=Sphericity ($E_\mathrm{CMS}=200$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d148-x01-y01
Title=Sphericity ($E_\mathrm{CMS}=206$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d149-x01-y01
Title=Durham jet resolution $2 \to 1$ ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$-\ln(y_{12})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{12})$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d150-x01-y01
Title=Durham jet resolution $2 \to 1$ ($E_\mathrm{CMS}=133$ GeV)
XLabel=$-\ln(y_{12})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{12})$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d151-x01-y01
Title=Durham jet resolution $2 \to 1$ ($E_\mathrm{CMS}=161$ GeV)
XLabel=$-\ln(y_{12})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{12})$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d152-x01-y01
Title=Durham jet resolution $2 \to 1$ ($E_\mathrm{CMS}=173$ GeV)
XLabel=$-\ln(y_{12})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{12})$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d153-x01-y01
Title=Durham jet resolution $2 \to 1$ ($E_\mathrm{CMS}=183$ GeV)
XLabel=$-\ln(y_{12})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{12})$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d154-x01-y01
Title=Durham jet resolution $2 \to 1$ ($E_\mathrm{CMS}=189$ GeV)
XLabel=$-\ln(y_{12})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{12})$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d155-x01-y01
Title=Durham jet resolution $2 \to 1$ ($E_\mathrm{CMS}=200$ GeV)
XLabel=$-\ln(y_{12})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{12})$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d156-x01-y01
Title=Durham jet resolution $2 \to 1$ ($E_\mathrm{CMS}=206$ GeV)
XLabel=$-\ln(y_{12})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{12})$
LegendXPos=0.65
LegendYPos=0.8
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d157-x01-y01
Title=Durham jet resolution $3 \to 2$ ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$-\ln(y_{23})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{23})$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d158-x01-y01
Title=Durham jet resolution $3 \to 2$ ($E_\mathrm{CMS}=133$ GeV)
XLabel=$-\ln(y_{23})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{23})$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d159-x01-y01
Title=Durham jet resolution $3 \to 2$ ($E_\mathrm{CMS}=161$ GeV)
XLabel=$-\ln(y_{23})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{23})$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d160-x01-y01
Title=Durham jet resolution $3 \to 2$ ($E_\mathrm{CMS}=172$ GeV)
XLabel=$-\ln(y_{23})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{23})$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d161-x01-y01
Title=Durham jet resolution $3 \to 2$ ($E_\mathrm{CMS}=183$ GeV)
XLabel=$-\ln(y_{23})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{23})$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d162-x01-y01
Title=Durham jet resolution $3 \to 2$ ($E_\mathrm{CMS}=189$ GeV)
XLabel=$-\ln(y_{23})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{23})$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d163-x01-y01
Title=Durham jet resolution $3 \to 2$ ($E_\mathrm{CMS}=200$ GeV)
XLabel=$-\ln(y_{23})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{23})$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d164-x01-y01
Title=Durham jet resolution $3 \to 2$ ($E_\mathrm{CMS}=206$ GeV)
XLabel=$-\ln(y_{23})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{23})$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d165-x01-y01
Title=Durham jet resolution $4 \to 3$ ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$-\ln(y_{34})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{34})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d166-x01-y01
Title=Durham jet resolution $4 \to 3$ ($E_\mathrm{CMS}=133$ GeV)
XLabel=$-\ln(y_{34})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{34})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d167-x01-y01
Title=Durham jet resolution $4 \to 3$ ($E_\mathrm{CMS}=161$ GeV)
XLabel=$-\ln(y_{34})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{34})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d168-x01-y01
Title=Durham jet resolution $4 \to 3$ ($E_\mathrm{CMS}=172$ GeV)
XLabel=$-\ln(y_{34})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{34})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d169-x01-y01
Title=Durham jet resolution $4 \to 3$ ($E_\mathrm{CMS}=183$ GeV)
XLabel=$-\ln(y_{34})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{34})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d170-x01-y01
Title=Durham jet resolution $4 \to 3$ ($E_\mathrm{CMS}=189$ GeV)
XLabel=$-\ln(y_{34})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{34})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d171-x01-y01
Title=Durham jet resolution $4 \to 3$ ($E_\mathrm{CMS}=200$ GeV)
XLabel=$-\ln(y_{34})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{34})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d172-x01-y01
Title=Durham jet resolution $4 \to 3$ ($E_\mathrm{CMS}=206$ GeV)
XLabel=$-\ln(y_{34})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{34})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d173-x01-y01
Title=Durham jet resolution $5 \to 4$ ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$-\ln(y_{45})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{45})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d174-x01-y01
Title=Durham jet resolution $5 \to 4$ ($E_\mathrm{CMS}=133$ GeV)
XLabel=$-\ln(y_{45})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{45})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d175-x01-y01
Title=Durham jet resolution $5 \to 4$ ($E_\mathrm{CMS}=161$ GeV)
XLabel=$-\ln(y_{45})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{45})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d176-x01-y01
Title=Durham jet resolution $5 \to 4$ ($E_\mathrm{CMS}=172$ GeV)
XLabel=$-\ln(y_{45})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{45})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d177-x01-y01
Title=Durham jet resolution $5 \to 4$ ($E_\mathrm{CMS}=183$ GeV)
XLabel=$-\ln(y_{45})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{45})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d178-x01-y01
Title=Durham jet resolution $5 \to 4$ ($E_\mathrm{CMS}=189$ GeV)
XLabel=$-\ln(y_{45})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{45})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d179-x01-y01
Title=Durham jet resolution $5 \to 4$ ($E_\mathrm{CMS}=200$ GeV)
XLabel=$-\ln(y_{45})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{45})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d180-x01-y01
Title=Durham jet resolution $6 \to 5$ ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$-\ln(y_{56})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{56})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d181-x01-y01
Title=Durham jet resolution $6 \to 5$ ($E_\mathrm{CMS}=133$ GeV)
XLabel=$-\ln(y_{56})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{56})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d182-x01-y01
Title=Durham jet resolution $6 \to 5$ ($E_\mathrm{CMS}=161$ GeV)
XLabel=$-\ln(y_{56})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{56})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d183-x01-y01
Title=Durham jet resolution $6 \to 5$ ($E_\mathrm{CMS}=172$ GeV)
XLabel=$-\ln(y_{56})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{56})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d184-x01-y01
Title=Durham jet resolution $6 \to 5$ ($E_\mathrm{CMS}=183$ GeV)
XLabel=$-\ln(y_{56})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{56})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d185-x01-y01
Title=Durham jet resolution $6 \to 5$ ($E_\mathrm{CMS}=189$ GeV)
XLabel=$-\ln(y_{56})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{56})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d186-x01-y01
Title=Durham jet resolution $6 \to 5$ ($E_\mathrm{CMS}=200$ GeV)
XLabel=$-\ln(y_{56})$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\ln(y_{56})$
LegendXPos=0.25
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d187-x01-y01
Title=1-jet fraction ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$\ln(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{1~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d188-x01-y01
Title=1-jet fraction ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{1~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.8
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d189-x01-y01
Title=1-jet fraction ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{1~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.8
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d190-x01-y01
Title=1-jet fraction ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{1~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.8
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d191-x01-y01
Title=1-jet fraction ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{1~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.8
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d192-x01-y01
Title=1-jet fraction ($E_\mathrm{CMS}=189$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{1~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.8
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d193-x01-y01
Title=1-jet fraction ($E_\mathrm{CMS}=200$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{1~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.8
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d194-x01-y01
Title=1-jet fraction ($E_\mathrm{CMS}=206$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{1~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.8
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d195-x01-y01
Title=2-jet fraction ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$\ln(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{2~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d196-x01-y01
Title=2-jet fraction ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{2~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d197-x01-y01
Title=2-jet fraction ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{2~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d198-x01-y01
Title=2-jet fraction ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{2~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d199-x01-y01
Title=2-jet fraction ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{2~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d200-x01-y01
Title=2-jet fraction ($E_\mathrm{CMS}=189$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{2~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d201-x01-y01
Title=2-jet fraction ($E_\mathrm{CMS}=200$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{2~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d202-x01-y01
Title=2-jet fraction ($E_\mathrm{CMS}=206$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{2~jet})/\sigma(\mathrm{inclusive})$
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d203-x01-y01
Title=3-jet fraction ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$\ln(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{3~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.25
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d204-x01-y01
Title=3-jet fraction ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{3~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.25
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d205-x01-y01
Title=3-jet fraction ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{3~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.25
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d206-x01-y01
Title=3-jet fraction ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{3~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.25
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d207-x01-y01
Title=3-jet fraction ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{3~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.25
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d208-x01-y01
Title=3-jet fraction ($E_\mathrm{CMS}=189$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{3~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.25
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d209-x01-y01
Title=3-jet fraction ($E_\mathrm{CMS}=200$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{3~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.25
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d210-x01-y01
Title=3-jet fraction ($E_\mathrm{CMS}=206$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{3~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.25
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d211-x01-y01
Title=4-jet fraction ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$\ln(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{4~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d212-x01-y01
Title=4-jet fraction ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{4~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d213-x01-y01
Title=4-jet fraction ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{4~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d214-x01-y01
Title=4-jet fraction ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{4~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d215-x01-y01
Title=4-jet fraction ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{4~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d216-x01-y01
Title=4-jet fraction ($E_\mathrm{CMS}=189$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{4~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d217-x01-y01
Title=4-jet fraction ($E_\mathrm{CMS}=200$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{4~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d218-x01-y01
Title=4-jet fraction ($E_\mathrm{CMS}=206$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{4~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d219-x01-y01
Title=5-jet fraction ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$\ln(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{5~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.15
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d220-x01-y01
Title=5-jet fraction ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{5~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d221-x01-y01
Title=5-jet fraction ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{5~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d222-x01-y01
Title=5-jet fraction ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{5~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d223-x01-y01
Title=5-jet fraction ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{5~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d224-x01-y01
Title=5-jet fraction ($E_\mathrm{CMS}=189$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{5~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d225-x01-y01
Title=5-jet fraction ($E_\mathrm{CMS}=200$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{5~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d226-x01-y01
Title=5-jet fraction ($E_\mathrm{CMS}=206$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\mathrm{5~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.35
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d227-x01-y01
Title=$\geq$6-jet fraction ($E_\mathrm{CMS}=91.2$ GeV)
XLabel=$\ln(y_\mathrm{cut})$
YLabel=$\sigma(\geq\mathrm{6~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d228-x01-y01
Title=$\geq$6-jet fraction ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\geq\mathrm{6~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d229-x01-y01
Title=$\geq$6-jet fraction ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\geq\mathrm{6~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d230-x01-y01
Title=$\geq$6-jet fraction ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\geq\mathrm{6~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d231-x01-y01
Title=$\geq$6-jet fraction ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\geq\mathrm{6~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d232-x01-y01
Title=$\geq$6-jet fraction ($E_\mathrm{CMS}=189$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\geq\mathrm{6~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d233-x01-y01
Title=$\geq$6-jet fraction ($E_\mathrm{CMS}=200$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\geq\mathrm{6~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /ALEPH_2004_I636645/d234-x01-y01
Title=$\geq$6-jet fraction ($E_\mathrm{CMS}=206$ GeV)
XLabel=$\log(y_\mathrm{cut})$
YLabel=$\sigma(\geq\mathrm{6~jet})/\sigma(\mathrm{inclusive})$
LegendXPos=0.05
LegendYPos=0.6
# END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d01-x01-y01
Title=Charged Particle Multiplicity in $e^+e^-\to q\bar{q}$
XLabel=$\sqrt{s}$
YLabel=$N_{\mathrm{charged}}$
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d02-x01-y01
Title=$\pi^\pm$ Multiplicity in $e^+e^-\to q\bar{q}$
XLabel=$\sqrt{s}$
YLabel=$N_{\pi^\pm}$
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d02-x01-y02
Title=$K^\pm$ Multiplicity in $e^+e^-\to q\bar{q}$
XLabel=$\sqrt{s}$
YLabel=$N_{K^\pm}$
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d02-x01-y03
Title=$K^0$ Multiplicity in $e^+e^-\to q\bar{q}$
XLabel=$\sqrt{s}$
YLabel=$N_{K^0}$
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d02-x01-y04
Title=Proton Multiplicity in $e^+e^-\to q\bar{q}$
XLabel=$\sqrt{s}$
YLabel=$N_{p}$
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d02-x01-y05
Title=$\Lambda$ Multiplicity in $e^+e^-\to q\bar{q}$
XLabel=$\sqrt{s}$
YLabel=$N_{\Lambda}$
ConnectGaps=1
LogY=0
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d03-x01-y01
Title=Charged multiplicity in $WW$ events
YLabel=$N_{\text{charged}}$
XCustomMajorTicks=1.  $183(4Q)$     2.   $189(4Q)$   3.   $183(2Q)$   4.   $189(2Q)$
XLabel=$\sqrt{s}$ [GeV]
LogY=0
XMin=0.5
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d04-x01-y01
Title=$\pi^\pm$ multiplicity in hadronic $WW$ (4Q)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$N_{\pi^\pm}$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d04-x01-y02
Title=$\pi^\pm$ multiplicity in semi-leptonic $WW$ (2Q)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$N_{\pi^\pm}$
LogY=0
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d04-x01-y03
Title=$K^\pm$ multiplicity in hadronic $WW$ (4Q)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$N_{K^\pm}$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d04-x01-y04
Title=$K^\pm$ multiplicity in semi-leptonic $WW$ (2Q)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$N_{K^\pm}$
LogY=0
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d04-x01-y05
Title=Proton multiplicity in hadronic $WW$ (4Q)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$N_{p}$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d04-x01-y06
Title=Proton multiplicity in semi-leptonic $WW$ (2Q)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$N_{p}$
LogY=0
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d05-x01-y01
Title=Charged  momentum for hadronic $WW$ (4Q) at 189 GeV
XLabel=$p$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p$  [$\text{GeV}^{-1}$]
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d05-x01-y02
Title=Charged  momentum for semi-leptonic $WW$ (2Q) at 189 GeV
XLabel=$p$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p$  [$\text{GeV}^{-1}$]
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d06-x01-y01
Title=Difference of Charged  momentum  for $WW$ (4Q- 2$\times$2Q) at 189 GeV
XLabel=$p$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p$  [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d07-x01-y01
Title=Charged  momentum for hadronic $WW$ (4Q) at 183 GeV
XLabel=$p$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p$  [$\text{GeV}^{-1}$]
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d07-x01-y02
Title=Charged  momentum for semi-leptonic $WW$ (2Q) at 183 GeV
XLabel=$p$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p$  [$\text{GeV}^{-1}$]
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d08-x01-y01
Title=Difference of Charged  momentum dfor $WW$ (4Q- 2$\times$2Q) at 183 GeV
XLabel=$p$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p$  [$\text{GeV}^{-1}$]
LogY=0
END PLOT


BEGIN PLOT /DELPHI_2001_I526164/d09-x01-y01
Title=Charged Particle $\xi$ for hadronic $WW$ (4Q) at 189 GeV
XLabel=$\xi=-\log(2p/\sqrt{s})$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d09-x01-y02
Title=Charged Particle $\xi$ for semi-leptonic $WW$ (2Q) at 189 GeV
XLabel=$\xi=-\log(2p/\sqrt{s})$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d09-x01-y03
Title=Difference of Charged Particle $\xi$  for $WW$ (4Q- 2$\times$2Q) at 189 GeV
XLabel=$\xi=-\log(2p/\sqrt{s})$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=0
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d10-x01-y01
Title=Charged Particle $\xi$ for hadronic $WW$ (4Q) at 183 GeV
XLabel=$\xi=-\log(2p/\sqrt{s})$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d10-x01-y02
Title=Charged Particle $\xi$ for semi-leptonic $WW$ (2Q) at 183 GeV
XLabel=$\xi=-\log(2p/\sqrt{s})$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d10-x01-y03
Title=Difference of Charged Particle $\xi$  for $WW$ (4Q- 2$\times$2Q) at 183 GeV
XLabel=$\xi=-\log(2p/\sqrt{s})$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=0
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d11-x01-y01
Title=Charged  particle $p_\perp$ for hadronic $WW$ (4Q) at 189 GeV
XLabel=$p_\perp$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p_\perp$  [$\text{GeV}^{-1}$]
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d11-x01-y02
Title=Charged  particle $p_\perp$ for semi-leptonic $WW$ (2Q) at 189 GeV
XLabel=$p_\perp$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p_\perp$  [$\text{GeV}^{-1}$]
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d11-x01-y03
Title=Difference of Charged  particle $p_\perp$  for $WW$ (4Q- 2$\times$2Q) at 189 GeV
XLabel=$p_\perp$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p_\perp$  [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d12-x01-y01
Title=Charged  particle $p_\perp$ for hadronic $WW$ (4Q) at 183 GeV
XLabel=$p_\perp$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p_\perp$  [$\text{GeV}^{-1}$]
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d12-x01-y02
Title=Charged  particle $p_\perp$ for semi-leptonic $WW$ (2Q) at 183 GeV
XLabel=$p_\perp$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p_\perp$  [$\text{GeV}^{-1}$]
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d12-x01-y03
Title=Difference of Charged  particle $p_\perp$  for $WW$ (4Q- 2$\times$2Q) at 183 GeV
XLabel=$p_\perp$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}p_\perp$  [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d13-x01-y01
Title=Charged particle $\xi$ for hadronic $WW$ (4Q) at 189 GeV
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d13-x01-y02
Title=$\pi^\pm$ $\xi$ for hadronic $WW$ (4Q) at 189 GeV
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d13-x01-y03
Title=$K^\pm$ $\xi$ for hadronic $WW$ (4Q) at 189 GeV
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d13-x01-y04
Title=Proton $\xi$ for hadronic $WW$ (4Q) at 189 GeV
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d14-x01-y01
Title=Charged particle $\xi$ for semi-leptonic $WW$ (2Q) at 189 GeV
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d14-x01-y02
Title=$\pi^\pm$ $\xi$ for semi-leptonic $WW$ (2Q) at 189 GeV
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d14-x01-y03
Title=$K^\pm$ $\xi$ for semi-leptonic $WW$ (2Q) at 189 GeV
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d14-x01-y04
Title=Proton $\xi$ for semi-leptonic $WW$ (2Q) at 189 GeV
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d15-x01-y01
Title=Charged particle $\xi$ for semi-leptonic $WW$ (2Q) at 189 GeV ($W$ rest frame)
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d15-x01-y02
Title=$\pi^\pm$ $\xi$ for semi-leptonic $WW$ (2Q) at 189 GeV ($W$ rest frame)
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d15-x01-y03
Title=$K^\pm$ $\xi$ for semi-leptonic $WW$ (2Q) at 189 GeV ($W$ rest frame)
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d15-x01-y04
Title=Proton $\xi$ for semi-leptonic $WW$ (2Q) at 189 GeV ($W$ rest frame)
XLabel=$\xi=-\log(2p/m_W)$ 
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
LogY=1
END PLOT

BEGIN PLOT /DELPHI_2001_I526164/d16-x01-y01
Title=Log Scaled momentum fraction for $K^0$ at 183 GeV
XLabel=$\xi=-\log(2p/m_W)$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d16-x01-y02
Title=Log Scaled momentum fraction for $K^0$ at 189 GeV
XLabel=$\xi=-\log(2p/m_W)$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d16-x01-y03
Title=Log Scaled momentum fraction for $\Lambda$ at 183 GeV
XLabel=$\xi=-\log(2p/m_W)$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
END PLOT
BEGIN PLOT /DELPHI_2001_I526164/d16-x01-y04
Title=Log Scaled momentum fraction for $\Lambda$ at 189 GeV
XLabel=$\xi=-\log(2p/m_W)$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\xi$
END PLOT

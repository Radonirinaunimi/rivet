BEGIN PLOT /BABAR_2011_I897848/d01-x01
Title=$\pi^-\pi^0$ mass  distribution in $B^0\to K^+\pi^-\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I897848/d02-x01
Title=$K^+\pi^0$ mass  distribution in $B^0\to K^+\pi^-\pi^0$
XLabel=$m_{K^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I897848/d03-x01
Title=$K^+\pi^-$ mass  distribution in $B^0\to K^+\pi^-\pi^0$
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

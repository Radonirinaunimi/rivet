BEGIN PLOT /LHCB_2022_I1960979/d01-x01-y01
Title=$\pi^+\pi^+\pi^-$ mass distribution in $B_c^+\to J/\psi \pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I1960979/d01-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $B_c^+\to J/\psi \pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I1960979/d02-x01-y01
Title=$K^-\pi^+$ mass distribution in $B_c^+\to J/\psi K^+K^-\pi^+$
XLabel=$m_{K^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I1960979/d02-x01-y02
Title=$K^+K^-$ mass distribution in $B_c^+\to J/\psi K^+K^-\pi^+$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
XMax=0.98
XMax=1.23
END PLOT
BEGIN PLOT /LHCB_2022_I1960979/d03-x01-y01
Title=$K^+\pi^-$ mass distribution in $B_c^+\to J/\psi K^+\pi^-\pi^+$
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2022_I1960979/d04-x01-y01
Title=$K^+K^-$ mass distribution in $B_c^+\to J/\psi K^+K^-K^+$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
XMax=1.00
XMax=1.08
END PLOT

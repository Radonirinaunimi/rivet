BEGIN PLOT /ARGUS_1987_I248655/d02-x01-y01
Title=Direct Photon Spectrum in $\Upsilon(1S)$ decay
XLabel=$x_\gamma$
YLabel=$1/N_{ggg} \text{d}N^\gamma/\text{d}x_\gamma$
LogY=0
END PLOT

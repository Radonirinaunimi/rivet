BEGIN PLOT /BELLE_2014_I1282136/d01-x01-y01
Title=$\pi^0K^0_SK^0_S$ mass distribution in $\tau^-\to\pi^-\pi^0K^0_SK^0_S$
XLabel=$m_{\pi^0K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1282136/d01-x01-y02
Title=$\pi^-K^0_S$ mass distribution in $\tau^-\to\pi^-\pi^0K^0_SK^0_S$
XLabel=$m_{\pi^-K^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-K^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1282136/d02-x01-y01
Title=$\pi^-\pi^0$ mass distribution in $\tau^-\to\pi^-\pi^0K^0_SK^0_S$
XLabel=$m_{\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1282136/d02-x01-y02
Title=$K^0_S\pi^0$ mass distribution in $\tau^-\to\pi^-\pi^0K^0_SK^0_S$
XLabel=$m_{K^0_S\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1282136/d02-x01-y03
Title=$K^0_SK^0_S$ mass distribution in $\tau^-\to\pi^-\pi^0K^0_SK^0_S$
XLabel=$m_{K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1282136/d02-x01-y04
Title=$K^0_SK^0_S\pi^-$ mass distribution in $\tau^-\to\pi^-\pi^0K^0_SK^0_S$
XLabel=$m_{K^0_SK^0_S\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1282136/d02-x01-y05
Title=$K^0_S\pi^0\pi^-$ mass distribution in $\tau^-\to\pi^-\pi^0K^0_SK^0_S$
XLabel=$m_{K^0_S\pi^0\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^0\pi^-}$ [$\text{GeV}^{-1}]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2019_I1729723/d01-x01-y01
Title=$K^-\pi^+$ mass distribution in $\bar{B}^0\to K^0_SK^-\pi^+$
XLabel=$m_{K^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$\text{Br}/\mathrm{d}m_{K^-\pi^+}$ [$10^{-7}\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2019_I1729723/d01-x01-y02
Title=$K^+\pi^-$ mass distribution in $\bar{B}^0\to K^0_SK^+\pi^-$
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$\text{Br}/\mathrm{d}m_{K^+\pi^-}$ [$10^{-7}\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2019_I1729723/d02-x01-y01
Title=$K^0_S\pi^+$ mass distribution in $\bar{B}^0\to K^0_SK^-\pi^+$
XLabel=$m_{K^0_S\pi^+}$ [$\mathrm{GeV}$]
YLabel=$\text{Br}/\mathrm{d}m_{K^0_S\pi^+}$ [$10^{-7}\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2019_I1729723/d02-x01-y02
Title=$K^0_S\pi^-$ mass distribution in $\bar{B}^0\to K^0_SK^+\pi^-$
XLabel=$m_{K^0_S\pi^-}$ [$\mathrm{GeV}$]
YLabel=$\text{Br}/\mathrm{d}m_{K^0_S\pi^-}$ [$10^{-7}\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2019_I1729723/d03-x01-y01
Title=$K^0_SK^-$ mass distribution in $\bar{B}^0\to K^0_SK^-\pi^+$
XLabel=$m_{K^0_SK^-}$ [$\mathrm{GeV}$]
YLabel=$\text{Br}/\mathrm{d}m_{K^0_SK^-}$ [$10^{-7}\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2019_I1729723/d03-x01-y02
Title=$K^0_SK^+$ mass distribution in $\bar{B}^0\to K^0_SK^+\pi^-$
XLabel=$m_{K^0_SK^+}$ [$\mathrm{GeV}$]
YLabel=$\text{Br}/\mathrm{d}m_{K^0_SK^+}$ [$10^{-7}\mathrm{GeV}^{-1}$]
LogY=0
END PLOT


# ** Gap fractions vs PtBar **

# BEGIN PLOT /ATLAS_2011_I917526/d0[12345]-x01-y0[12]
XLabel=$\overline{P_{T}}$ [GeV]
YLabel=Gap fraction
YMin=0.0
YMax=1.05
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d01-x01-y01
Title=Gap fraction vs $\overline{P_{T}}$ for $1.0<|\Delta{}y|<2.0$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d01-x01-y02
Title=Gap fraction vs $\overline{P_{T}}$ for $1.0<|\Delta{}y|<2.0$, Fwd/Bwd 
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d02-x01-y01
Title=Gap fraction vs $\overline{P_{T}}$ for $2.0<|\Delta{}y|<3.0$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d02-x01-y02
Title=Gap fraction vs $\overline{P_{T}}$ for $2.0<|\Delta{}y|<3.0$, Fwd/Bwd 
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d03-x01-y01
Title=Gap fraction vs $\overline{P_{T}}$ for $3.0<|\Delta{}y|<4.0$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d03-x01-y02
Title=Gap fraction vs $\overline{P_{T}}$ for $3.0<|\Delta{}y|<4.0$, Fwd/Bwd 
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d04-x01-y01
Title=Gap fraction vs $\overline{P_{T}}$ for $4.0<|\Delta{}y|<5.0$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d04-x01-y02
Title=Gap fraction vs $\overline{P_{T}}$ for $4.0<|\Delta{}y|<5.0$, Fwd/Bwd 
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d05-x01-y01
Title=Gap fraction vs $\overline{P_{T}}$ for $5.0<|\Delta{}y|<6.0$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d05-x01-y02
Title=Gap fraction vs $\overline{P_{T}}$ for $5.0<|\Delta{}y|<6.0$, Fwd/Bwd 
# END PLOT


# ** Gap fractions vs Delta Y **
# BEGIN PLOT /ATLAS_2011_I917526/d0[6789]-x01-y0[12]
XLabel=$|\Delta{}y|$
YLabel=Gap fraction
YMin=0.0
YMax=1.05
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d1[012]-x01-y0[12]
XLabel=$|\Delta{}y|$
YLabel=Gap fraction
YMin=0.0
YMax=1.05
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d1[9]-x01-y0[1]
XLabel=$|\Delta{}y|$
YLabel=Gap fraction
YMin=0.0
YMax=1.05
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d2[012345]-x01-y0[1]
XLabel=$|\Delta{}y|$
YLabel=Gap fraction
YMin=0.0
YMax=1.05
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d06-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $70<\overline{P_{T}}<90$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d06-x01-y02
Title=Gap fraction vs $|\Delta{}y|$ for $70<\overline{P_{T}}<90$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d07-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $90<\overline{P_{T}}<120$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d07-x01-y02
Title=Gap fraction vs $|\Delta{}y|$ for $90<\overline{P_{T}}<120$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d08-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $120<\overline{P_{T}}<150$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d08-x01-y02
Title=Gap fraction vs $|\Delta{}y|$ for $120<\overline{P_{T}}<150$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d09-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $150<\overline{P_{T}}<180$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d09-x01-y02
Title=Gap fraction vs $|\Delta{}y|$ for $150<\overline{P_{T}}<180$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d10-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $180<\overline{P_{T}}<210$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d10-x01-y02
Title=Gap fraction vs $|\Delta{}y|$ for $180<\overline{P_{T}}<210$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d11-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $210<\overline{P_{T}}<240$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d11-x01-y02
Title=Gap fraction vs $|\Delta{}y|$ for $210<\overline{P_{T}}<240$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d12-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $240<\overline{P_{T}}<270$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d12-x01-y02
Title=Gap fraction vs $|\Delta{}y|$ for $240<\overline{P_{T}}<270$, Fwd/Bwd 
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d19-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $70<\overline{P_{T}}<90$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d20-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $90<\overline{P_{T}}<120$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d21-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $120<\overline{P_{T}}<150$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d22-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $150<\overline{P_{T}}<180$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d23-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $180<\overline{P_{T}}<210$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d24-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $210<\overline{P_{T}}<240$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d25-x01-y01
Title=Gap fraction vs $|\Delta{}y|$ for $240<\overline{P_{T}}<270$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT


# ** Gap fractions vs Q_0 **
# BEGIN PLOT /ATLAS_2011_I917526/d1[345678]-x01-y0[12]
XLabel=$Q_{0}$ [GeV]
YLabel=Gap fraction
YMin=0.0
YMax=1.05
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d13-x01-y01
Title=Gap fraction vs $Q_{0}$ for $70<\overline{P_{T}}<90$  $2<|\Delta{}y|<3$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d13-x01-y02
Title=Gap fraction vs $Q_{0}$ for $70<\overline{P_{T}}<90$  $2<|\Delta{}y|<3$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d14-x01-y01
Title=Gap fraction vs $Q_{0}$ for $70<\overline{P_{T}}<90$  $4<|\Delta{}y|<5$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d14-x01-y02
Title=Gap fraction vs $Q_{0}$ for $70<\overline{P_{T}}<90$  $4<|\Delta{}y|<5$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d15-x01-y01
Title=Gap fraction vs $Q_{0}$ for $120<\overline{P_{T}}<150$  $2<|\Delta{}y|<3$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d15-x01-y02
Title=Gap fraction vs $Q_{0}$ for $120<\overline{P_{T}}<150$  $2<|\Delta{}y|<3$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d16-x01-y01
Title=Gap fraction vs $Q_{0}$ for $120<\overline{P_{T}}<150$  $4<|\Delta{}y|<5$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d16-x01-y02
Title=Gap fraction vs $Q_{0}$ for $120<\overline{P_{T}}<150$  $4<|\Delta{}y|<5$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d17-x01-y01
Title=Gap fraction vs $Q_{0}$ for $210<\overline{P_{T}}<240$  $2<|\Delta{}y|<3$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d17-x01-y02
Title=Gap fraction vs $Q_{0}$ for $210<\overline{P_{T}}<240$  $2<|\Delta{}y|<3$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d18-x01-y01
Title=Gap fraction vs $Q_{0}$ for $210<\overline{P_{T}}<240$  $4<|\Delta{}y|<5$, Leading Jet
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d18-x01-y02
Title=Gap fraction vs $Q_{0}$ for $210<\overline{P_{T}}<240$  $4<|\Delta{}y|<5$, Fwd/Bwd 
# END PLOT


# ** Average NJet vs PtBar **
# BEGIN PLOT /ATLAS_2011_I917526/d2[6789]-x01-y0[12]
XLabel=$\overline{P_{T}}$ [GeV]
YLabel=$\overline{N_{jet}}$
YMin=0.0
YMax=3.0
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d26-x01-y01
Title=$\overline{N_{jet}}$ vs $\overline{P_{T}}$ for $1<|\Delta{}y|<2$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d26-x01-y02
Title=$\overline{N_{jet}}$ vs $\overline{P_{T}}$ for $1<|\Delta{}y|<2$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d27-x01-y01
Title=$\overline{N_{jet}}$ vs $\overline{P_{T}}$ for $2<|\Delta{}y|<3$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d27-x01-y02
Title=$\overline{N_{jet}}$ vs $\overline{P_{T}}$ for $2<|\Delta{}y|<3$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d28-x01-y01
Title=$\overline{N_{jet}}$ vs $\overline{P_{T}}$ for $3<|\Delta{}y|<4$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d28-x01-y02
Title=$\overline{N_{jet}}$ vs $\overline{P_{T}}$ for $3<|\Delta{}y|<4$, Fwd/Bwd 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d29-x01-y01
Title=$\overline{N_{jet}}$ vs $\overline{P_{T}}$ for $4<|\Delta{}y|<5$, Leading Jet 
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d29-x01-y02
Title=$\overline{N_{jet}}$ vs $\overline{P_{T}}$ for $4<|\Delta{}y|<5$, Fwd/Bwd 
# END PLOT



# ** Average NJet vs Delta Y **
# BEGIN PLOT /ATLAS_2011_I917526/d3[0123456]-x01-y0[1]
XLabel=$|\Delta{}y|$
YLabel=$\overline{N_{jet}}$
YMin=0.0
YMax=3.0
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d3[789]-x01-y0[12]
XLabel=$|\Delta{}y|$
YLabel=$\overline{N_{jet}}$
YMin=0.0
YMax=3.0
LogY=0
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d4[0123]-x01-y0[12]
XLabel=$|\Delta{}y|$
YLabel=$\overline{N_{jet}}$
YMin=0.0
YMax=3.0
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d30-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $70<\overline{P_{T}}<90$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d31-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $90<\overline{P_{T}}<120$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d32-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $120<\overline{P_{T}}<150$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d33-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $150<\overline{P_{T}}<180$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d34-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $180<\overline{P_{T}}<210$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d35-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $210<\overline{P_{T}}<240$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d36-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $240<\overline{P_{T}}<270$, Fwd/Bwd $Q_{0}=\overline{P_{T}}$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I917526/d37-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $70<\overline{P_{T}}<90$, Leading Jet
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d37-x01-y02
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $70<\overline{P_{T}}<90$, Fwd/Bwd
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d38-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $90<\overline{P_{T}}<120$, Leading Jet
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d38-x01-y02
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $90<\overline{P_{T}}<120$, Fwd/Bwd
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d39-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $120<\overline{P_{T}}<150$, Leading Jet
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d39-x01-y02
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $120<\overline{P_{T}}<150$, Fwd/Bwd
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d40-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $150<\overline{P_{T}}<180$, Leading Jet
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d40-x01-y02
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $150<\overline{P_{T}}<180$, Fwd/Bwd
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d41-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $180<\overline{P_{T}}<210$, Leading Jet
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d41-x01-y02
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $180<\overline{P_{T}}<210$, Fwd/Bwd
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d42-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $210<\overline{P_{T}}<240$, Leading Jet
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d42-x01-y02
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $210<\overline{P_{T}}<240$, Fwd/Bwd
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d43-x01-y01
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $240<\overline{P_{T}}<270$, Leading Jet
# END PLOT
# BEGIN PLOT /ATLAS_2011_I917526/d43-x01-y02
Title=$\overline{N_{jet}}$ vs $|\Delta{}y|$ for $240<\overline{P_{T}}<270$, Fwd/Bwd
# END PLOT

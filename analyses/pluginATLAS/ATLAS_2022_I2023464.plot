BEGIN PLOT /ATLAS_2022_I2023464/d01-x01-y01
Title=Inclusive xsection in the five fiducial regions
XLabel=Fiducial region
YLabel=$\sigma_{fid}$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d02-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma}$
XLabel=$p_{T}^{\gamma\gamma}$ [GeV]
YLabel=$d\sigma/dp_{T}^{\gamma\gamma}$ [fb/GeV]
LogX=1
XMin=1.0
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d03-x01-y01
Title=xsection vs $N_{jets}$
XLabel=$N_{jets}$
YLabel=$\sigma_{fid}$
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d04-x01-y01
Title=xsection vs $N_{b-jets}$ category
XLabel=$N(b-jets)$ category
YLabel=$\sigma_{fid}$
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d05-x01-y01
Title=1D differential xsection vs $p_{T}^{j1}$
XLabel=$p_{T}^{j1}$ [GeV]
YLabel=$d\sigma/dp_{T}^{j1}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d06-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma}$ ($p_{T}^{j1}<30$ GeV)
XLabel=$p_{T}^{\gamma\gamma}$ [GeV]
YLabel=$d\sigma/dp_{T}^{\gamma\gamma}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d07-x01-y01
Title=1D differential xsection vs $m_{jj}$
XLabel=$m_{jj}$ [GeV]
YLabel=$d\sigma/dm_{jj}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d08-x01-y01
Title=1D differential xsection vs $\Delta\phi_{jj}$
XLabel=$\Delta\phi_{jj}$
YLabel=$d\sigma/d\Delta\phi_{jj}$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d09-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma}$ in bins of $|y_{\gamma\gamma}|$
XLabel=Bin number
YLabel=$d\sigma/dp_{T}^{\gamma\gamma}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d10-x01-y01
Title=1D differential xsection vs $\Delta\phi_{jj}$ in VBF region
XLabel=$\Delta\phi_{jj}$
YLabel=$d\sigma/d\Delta\phi_{jj}$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d21-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma 1}/m_{\gamma\gamma}$
XLabel=$p_{T}^{\gamma 1}/m_{\gamma\gamma}$
YLabel=$d\sigma/dp_{T}^{\gamma 1}/m_{\gamma\gamma}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d23-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma 2}/m_{\gamma\gamma}$
XLabel=$p_{T}^{\gamma 2}/m_{\gamma\gamma}$
YLabel=$d\sigma/dp_{T}^{\gamma 2}/m_{\gamma\gamma}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d25-x01-y01
Title=1D differential xsection vs $|y_{\gamma\gamma}|$
XLabel=$|y_{\gamma\gamma}|$
YLabel=$|d\sigma/dy_{\gamma\gamma}|$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d27-x01-y01
Title=1D differential xsection vs $m_{\gamma\gamma j}$
XLabel=$m_{\gamma\gamma j}$ [GeV]
YLabel=$d\sigma/dm_{\gamma\gamma j}$ [fb/GeV]
LogX=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d29-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma j}$
XLabel=$p_{T}^{\gamma\gamma j}$ [GeV]
YLabel=$d\sigma/dp_{T}^{\gamma\gamma j}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d31-x01-y01
Title=1D differential xsection vs $H_T$
XLabel=$H_T$ [GeV]
YLabel=$d\sigma/dH_T$ [fb/GeV]
LogX=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d33-x01-y01
Title=1D differential xsection vs $\tau_{C,j1}$
XLabel=$\tau_{C,j1}$ [GeV]
YLabel=$d\sigma/d\tau_{C,j1}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d35-x01-y01
Title=1D differential xsection vs $\Sigma\tau_{C,j}$
XLabel=$\Sigma\tau_{C,j}$ [GeV]
YLabel=$d\sigma/d\Sigma\tau_{C,j}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d37-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma}$ ($p_{T}^{j1}<40$ GeV)
XLabel=$p_{T}^{\gamma\gamma}$ [GeV]
YLabel=$d\sigma/dp_{T}^{\gamma\gamma}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d39-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma}$ ($p_{T}^{j1}<50$ GeV)
XLabel=$p_{T}^{\gamma\gamma}$ [GeV]
YLabel=$d\sigma/dp_{T}^{\gamma\gamma}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d41-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma}$ ($p_{T}^{j1}<60$ GeV)
XLabel=$p_{T}^{\gamma\gamma}$ [GeV]
YLabel=$d\sigma/dp_{T}^{\gamma\gamma}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d43-x01-y01
Title=1D differential xsection vs $|\Delta\phi^{\gamma\gamma,jj}|$
XLabel=$\pi-|\Delta\phi^{\gamma\gamma,jj}|$
YLabel=$d\sigma/d|\Delta\phi|$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d45-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma jj}$
XLabel= $p_{T}^{\gamma\gamma jj}$ [GeV]
YLabel=$d\sigma/dp_{T}^{\gamma\gamma jj}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d47-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma}$ in bins of $p_{T}^{\gamma\gamma j}$
XLabel=Bin number
YLabel=$d\sigma/dp_{T}^{\gamma\gamma}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d49-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma}$ in bins of $\tau_{C,j1}$
XLabel=Bin number
YLabel=$d\sigma/dp_{T}^{\gamma\gamma}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d51-x01-y01
Title=1D differential xsection vs $(p_{T}^{\gamma 1}-p_{T}^{\gamma 2})/m_{\gamma\gamma}$ in bins of $(p_{T}^{\gamma 1}+p_{T}^{\gamma 2})/m_{\gamma\gamma}$
XLabel=$(p_{T}^{\gamma 1}-p_{T}^{\gamma 2})/m_{\gamma\gamma}$
YLabel=$d\sigma/d(p_{T}^{\gamma 1}-p_{T}^{\gamma 2})/m_{\gamma\gamma}$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d53-x01-y01
Title=1D differential xsection vs $|\eta^*|$ in VBF region
XLabel=$|\eta^*|$
YLabel=$d\sigma/d|\eta^*|$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d55-x01-y01
Title=1D differential xsection vs $p_{T}^{\gamma\gamma jj}$ in VBF region
XLabel=$p_{T}^{\gamma\gamma jj}$ [GeV]
YLabel=$d\sigma/dp_{T}^{\gamma\gamma jj}$ [fb/GeV]
LogX=1
XMin=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d57-x01-y01
Title=1D differential xsection vs $p_{T}^{j1}$ in VBF region
XLabel=$p_{T}^{j1}$ [GeV]
YLabel=$d\sigma/dp_{T}^{j1}$ [fb/GeV]
LogX=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2023464/d59-x01-y01
Title=1D differential xsection vs $p_{T}^{j1}$ in bins of $\Delta\phi_{jj}$ in VBF region
XLabel=Bin number
YLabel=$d\sigma/dp_{T}^{j1}$ [fb/GeV]
END PLOT

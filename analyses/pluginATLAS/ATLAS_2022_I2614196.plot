# BEGIN PLOT /ATLAS_2022_I2614196/.*
XTwosidedTicks=1
YTwosidedTicks=1
Legend=1
LegendAlign=l
LegendXPos=0.5
LegendYPos=0.9
RatioPlotYMin=0.8
RatioPlotYMax=1.2
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d01
XLabel=$p_\mathrm{T}^{\ell\ell}$ [GeV]
YLabel=$\mathrm{d}\sigma/\,\mathrm{d} p_\mathrm{T}^{\ell\ell}$ [fb / GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d02
XLabel=$p_\mathrm{T}^{\ell\ell} - p_\mathrm{T}^\gamma$ [GeV]
YLabel=$\mathrm{d}\sigma/\,\mathrm{d} (p_\mathrm{T}^{\ell\ell} - p_\mathrm{T}^\gamma)$ [fb / GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d03
XLabel=$p_\mathrm{T}^{\ell\ell} + p_\mathrm{T}^\gamma$ [GeV]
YLabel=$\mathrm{d}\sigma/\,\mathrm{d} (p_\mathrm{T}^{\ell\ell} + p_\mathrm{T}^\gamma)$ [fb / GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d04
XLabel=$\Delta R (\ell, \ell)$
YLabel=$\mathrm{d}\sigma/\,\mathrm{d} \Delta R (\ell, \ell)$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d05
XLabel=$N_\mathrm{jets}$
YLabel=$\mathrm{d}\sigma/\,\mathrm{d}N_\mathrm{jets}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d06
XLabel=$p_\mathrm{T}^\mathrm{Jet1}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^\mathrm{Jet1}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d07
XLabel=$p_\mathrm{T}^\mathrm{Jet2}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^\mathrm{Jet2}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d08
XLabel=$p_\mathrm{T}^{Jet2}/p_\mathrm{T}^{Jet1}$
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^\mathrm{Jet2}/p_\mathrm{T}^\mathrm{Jet1}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d09
XLabel=$m_{jj}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} m_{jj}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d10
XLabel=$m_{ll\gamma j}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} m_{ll\gamma j}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d11
XLabel=$H_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} H_\mathrm{T}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d12
XLabel=$p_\mathrm{T}^\gamma / \sqrt{H_\mathrm{T}}$ [$\sqrt{GeV}$]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^\gamma / \sqrt{H_\mathrm{T}}$ [fb / $\sqrt{GeV}$]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d13
XLabel=$\Delta\phi (\mathrm{Jet}, \gamma)$
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} \Delta\phi (\mathrm{Jet}, \gamma)$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d14
XLabel=$p_\mathrm{T}^{\ell \ell \gamma j}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^{\ell \ell \gamma j}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d15
Title=$p_\mathrm{T}^{\ell\ell} <$ 35 GeV
XLabel=$\phi_\mathrm{CS}$
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} \phi_\mathrm{CS}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d16
Title=$p_\mathrm{T}^{\ell\ell} <$ 35 GeV
XLabel=$\cos\theta_\mathrm{CS}$
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} \cos\theta_\mathrm{CS}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d..-x02
Title=35 GeV $< p_\mathrm{T}^{\ell\ell} <$ 60 GeV
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d..-x03
Title=60 GeV $< p_\mathrm{T}^{\ell\ell} <$ 90 GeV
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d..-x04
Title=90 GeV $< p_\mathrm{T}^{\ell\ell} <$ 135 GeV
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d..-x05
Title=135 GeV $< p_\mathrm{T}^{\ell\ell}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d17
Title=$m_{\ell \ell\gamma} <$ 200 GeV slice
XLabel=$p_\mathrm{T}^{\ell\ell\gamma} / m_{\ell\ell\gamma}$
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^{\ell\ell\gamma} / m_{\ell\ell\gamma}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d18
Title=200 GeV $< m_{\ell \ell\gamma} <$ 300 GeV slice
XLabel=$p_\mathrm{T}^{\ell\ell\gamma} / m_{\ell\ell\gamma}$
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^{\ell\ell\gamma} / m_{\ell\ell\gamma}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d19
Title=300 GeV $< m_{\ell \ell\gamma}$ slice
XLabel=$p_\mathrm{T}^{\ell\ell\gamma} / m_{\ell\ell\gamma}$
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^{\ell\ell\gamma} / m_{\ell\ell\gamma}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d20
Title=$p_\mathrm{T}^{\ell\ell} + p_\mathrm{T}^\gamma <$ 200 GeV slice
XLabel=$p_\mathrm{T}^{\ell\ell} - p_\mathrm{T}^\gamma$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} (p_\mathrm{T}^{\ell\ell} - p_\mathrm{T}^\gamma)$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d21
Title=200 GeV $< p_\mathrm{T}^{\ell\ell} + p_\mathrm{T}^\gamma <$ 300 GeV slice
XLabel=$p_\mathrm{T}^{\ell\ell} - p_\mathrm{T}^\gamma$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} (p_\mathrm{T}^{\ell\ell} - p_\mathrm{T}^\gamma)$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d22
Title=300 GeV $< p_\mathrm{T}^{\ell\ell} + p_\mathrm{T}^\gamma$ slice
XLabel=$p_\mathrm{T}^{\ell\ell} - p_\mathrm{T}^\gamma$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} (p_\mathrm{T}^{\ell\ell} - p_\mathrm{T}^\gamma)$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d23
Title=$p_\mathrm{T}^{\ell\ell\gamma} <$ 50 GeV slice
XLabel=$p_\mathrm{T}^{\ell\ell\gamma j}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^{\ell\ell\gamma j}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d24
Title=50 GeV $< p_\mathrm{T}^{\ell\ell\gamma} <$ 75 GeV slice
XLabel=$p_\mathrm{T}^{\ell\ell\gamma j}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^{\ell\ell\gamma j}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2614196/d25
Title=75 GeV $< p_\mathrm{T}^{\ell\ell\gamma}$ slice
XLabel=$p_\mathrm{T}^{\ell\ell\gamma j}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\mathrm{d} p_\mathrm{T}^{\ell\ell\gamma j}$ [fb/GeV]
# END PLOT


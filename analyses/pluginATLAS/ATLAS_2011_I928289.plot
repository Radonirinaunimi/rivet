# BEGIN PLOT /ATLAS_2011_I928289/d..-x..-y..
LeftMargin=1.5
LogX=0
LogY=0
GofType=chi2
GofLegend=0
LegendYPos=0.25
LegendXPos=0.05
RatioPlotYMin=0.85
RatioPlotYMax=1.15
XLabel=$|\eta_\ell|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta_\ell$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d07-x..-y..
YLabel=$A_\ell$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d01-x01-y01
Title=$Z\rightarrow ee$, bare level
YLabel=$\mathrm{d}\sigma / \mathrm{d}y_{\ell\ell}$ [pb]
XLabel=$|y_{\ell\ell}|$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d01-x01-y02
Title=$Z\rightarrow ee$, dressed level
YLabel=$\mathrm{d}\sigma / \mathrm{d}y_{\ell\ell}$ [pb]
XLabel=$|y_{\ell\ell}|$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d01-x01-y03
Title=$Z\rightarrow \mu\mu$, bare level
YLabel=$\mathrm{d}\sigma / \mathrm{d}y_{\ell\ell}$ [pb]
XLabel=$|y_{\ell\ell}|$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d01-x01-y04
Title=$Z\rightarrow \mu\mu$, dressed level
YLabel=$\mathrm{d}\sigma / \mathrm{d}y_{\ell\ell}$ [pb]
XLabel=$|y_{\ell\ell}|$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d03-x01-y01
Title=$W^-\rightarrow e^-\bar{\nu}$, bare level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d03-x01-y02
Title=$W^-\rightarrow e^-\bar{\nu}$, dressed level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d03-x01-y03
Title=$W^-\rightarrow \mu^-\bar{\nu}$, bare level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d03-x01-y04
Title=$W^-\rightarrow \mu^-\bar{\nu}$, dressed level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d05-x01-y01
Title=$W^+\rightarrow e^+\nu$, bare level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d05-x01-y02
Title=$W^+\rightarrow e^+\nu$, dressed level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d05-x01-y03
Title=$W^+\rightarrow \mu^+\nu$, bare level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d05-x01-y04
Title=$W^+\rightarrow \mu^+\nu$, dressed level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d07-x01-y01
Title=$W$ charge asymmetry, electron channel, bare level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d07-x01-y02
Title=$W$ charge asymmetry, electron channel, dressed level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d07-x01-y03
Title=$W$ charge asymmetry, muon channel, bare level
# END PLOT

# BEGIN PLOT /ATLAS_2011_I928289/d07-x01-y04
Title=$W$ charge asymmetry, muon channel, dressed level
# END PLOT


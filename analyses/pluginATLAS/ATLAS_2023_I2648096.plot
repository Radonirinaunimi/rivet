# BEGIN PLOT /ATLAS_2023_I2648096/*
XTwosidedTicks=1
YTwosidedTicks=1
LegendYPos=0.90
LegendXPos=0.95
LegendAlign=r
LogY=1
Title=
RatioPlotYMax=1.02
RatioPlotYMin=0.98
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d01
XLabel=Lepton $p_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d02
XLabel=Lepton $p_\mathrm{T}$ [GeV]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d03
LogY=0
XLabel=Lepton $|\eta|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\eta|$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d04
LogY=0
XLabel=Lepton $|\eta|$
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d05
XLabel=Dilepton $p_\mathrm{T}^{e\mu}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}^{e\mu}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d06
XLabel=Dilepton $p_\mathrm{T}^{e\mu}$ [GeV]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}^{e\mu}$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d07
XLabel=Dilepton $m^{e\mu}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} m^{e\mu}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d08
XLabel=Dilepton $m^{e\mu}$ [GeV]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} m^{e\mu}$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d09
XLabel=Dilepton $|y^{e\mu}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d} |y^{e\mu}|$ [fb]
LegendYPos=0.55
LegendXPos=0.05
LegendAlign=l
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d10
XLabel=Dilepton $|y^{e\mu}|$
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} |y^{e\mu}|$
LegendYPos=0.55
LegendXPos=0.05
LegendAlign=l
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d11
XLabel=Dilepton $\Delta \phi^{e\mu}$ [rad]
YLabel=$\mathrm{d}\sigma / \mathrm{d} \Delta \phi^{e\mu}$ [fb/rad]
LegendXPos=0.05
LegendAlign=l
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d12
XLabel=Dilepton $\Delta \phi^{e\mu}$ [rad]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} \Delta \phi^{e\mu}$ [1/rad]
LegendXPos=0.05
LegendAlign=l
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d13
XLabel=Dilepton $p_\mathrm{T}^{e} + p_\mathrm{T}^{\mu}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} ( p_\mathrm{T}^{e} + p_\mathrm{T}^{\mu} )$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d14
XLabel=Dilepton $p_\mathrm{T}^{e} + p_\mathrm{T}^{\mu}$ [GeV]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} ( p_\mathrm{T}^{e} + p_\mathrm{T}^{\mu} )$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d15
XLabel=Dilepton $E^{e} + E^{\mu}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} ( E^{e} + E^{\mu} )$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d16
XLabel=Dilepton $E^{e} + E^{\mu}$ [GeV]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} ( E^{e} + E^{\mu} )$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d17
LogY=1
XLabel=Lepton $|\eta|\times m^{e\mu}$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|\eta|\mathrm{d}m^{e\mu}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d18
LogY=1
XLabel=Lepton $|\eta|\times m^{e\mu}$
YLabel=$1 / \sigma \; \mathrm{d}^2\sigma / \mathrm{d}|\eta|\mathrm{d}m^{e\mu}$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d19
LogY=1
XLabel=Dilepton $|y^{e\mu}|\times m^{e\mu}$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{e\mu}|\mathrm{d}m^{e\mu}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d20
LogY=1
XLabel=Dilepton $|y^{e\mu}|\times m^{e\mu}$
YLabel=$1 / \sigma \; \mathrm{d}^2\sigma / \mathrm{d}|y^{e\mu}|\mathrm{d}m^{e\mu}$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d21
LogY=1
XLabel=Dilepton $\Delta \phi^{e\mu}\times m^{e\mu}$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}\Delta \phi^{e\mu}\mathrm{d}m^{e\mu}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d22
LogY=1
XLabel=Dilepton $\Delta \phi^{e\mu}\times m^{e\mu}$
YLabel=$1 / \sigma \; \mathrm{d}^2\sigma / \mathrm{d}\Delta \phi{e\mu}\mathrm{d}m^{e\mu}$
# END PLOT

# BEGIN PLOT /H1_1998_I477556/d01-x01-y01
XLabel=$p_{\mathrm{T}}$ [GeV/c]
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}\eta\mathrm{d}p^2_{\mathrm{T}}$ [nb]
# END PLOT

# BEGIN PLOT /H1_1998_I477556/d02-x01-y01
XLabel=$\eta$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta$ [nb]
CustomLegend=$ p_{\mathrm{T}} > 2.0$ GeV/c
LogY=0
# END PLOT

# BEGIN PLOT /H1_1998_I477556/d02-x01-y02
XLabel=$\eta$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta$ [nb]
CustomLegend=$ p_{\mathrm{T}} > 3.0$ GeV/c
LogY=0
# END PLOT

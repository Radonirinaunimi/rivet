from . import ALICE_2014_I1244523 # pseudo-2D object should really be 1D
from . import ATLAS_2018_I1711223 # use string edges
from . import ATLAS_2022_I2614196 # slice 2D object into 1D objects
from . import ATLAS_2023_I2648096 # slice long 1D object into sub 1D objects
from . import BABAR_2016_I1391152 # add seprate table for the "average" bin
from . import CELLO_1983_I191415 # remove extra bin in overlap region
from . import CLEO_1985_I205668 # mask rogue bin
from . import CMS_2017_I1631985 # add a vector<1D> versions of the 2D AOs
#from . import DELPHI_1993_I356732 # edges apparently completely wrong
from . import HRS_1987_I215848 # transform x-edges
from . import JADE_1990_I282847 # pseudo-2D object should really be 1D
from . import L3_1992_I334954 # mask "integral" bin
from . import MARKII_1985_I207785 # pseudo-2D object should really be 1D
from . import MARKII_1988_I246184 # mask last mega bin
from . import NUSEA_2003_I613362 # pseudo-3D object should really be 1D
from . import OPAL_1993_I342766 # pseudo-2D object should really be 1D
#from . import OPAL_1997_I440103 # lots of tables missing on HepData
#from . import TASSO_1984_I194774 # d01 values missing on HepData
from . import TASSO_1986_I230950 # transform x-edges
from . import TPC_1985_I205868 # pseudo-2D object should really be 1D
